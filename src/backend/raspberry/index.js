import Usb from 'usb';
import Fs from 'fs';
import crypto from 'crypto';
import Rimraf from 'rimraf';
import fetch from 'node-fetch';

import { CheckInsertedUsbPath, RenameUsbLabelToKumu, GetFullPathUsbInfo } from '../usb';
import { FunctionsServer, RaspberryConstant } from '../utils/constant';
import { AESEncrypt, AESDecrypt } from '../utils/encryptation';
import actions from '../utils/action';
import { castSpell } from '../emilia';
import { spells } from '../emilia/spells';

const drivelist = require('drivelist');
const dataDirPath = `${__dirname}/../data/`;

export default class Raspberry {
  constructor(props) {
    const { wsServer } = props;
    this.state = {
      configDataSaved: false,
      usbPath: '',
      config: false,
      serverStarted: false,
    };
    this.wsServer = wsServer;
    this.declareWebsocket();

    Usb.on('attach', (dev) => {
      this.handleUsbInserted();
    });
  }

  setState = (newState = {}, callback = () => {}) => {
    Object.keys(newState).forEach((key) => {
      this.state[key] = newState[key];
    });
    callback(this.state);
  };

  emitToWebsocket = (key, value = false) => {
    const { wsServer } = this;
    if (value) {
      wsServer.emit(key, value);
      return;
    }
    wsServer.emit(key);
  };

  declareWebsocket = () => {
    const { wsServer, state } = this;
    wsServer.on('connection', (socket) => {
      console.log('Socket connection start');
      const ip = socket.handshake.address;
      console.log('Ip:' + ip);
      this.handleUsbInserted();
      if (!state.serverStarted) {
        socket.on(actions.check_qr_code_raspberry, (json) => {
          this.checkRaspberryCode(json.data);
        });
        socket.on(actions.reset_raspberry_backend, (json) => {
          this.cleanConfigData();
          socket.send(actions.reset_raspberry_frontend);
        });
      }
    });
  };

  checkRaspberryCode = (qrHash) => {
    const { state } = this;
    const data = {
      raspberry_query_id: qrHash,
    };
    castSpell(spells.CHECK_IF_RASPBERRY_IS_ADDED, data)
      .then((myJson) => {
        // console.log(myJson);
        const key1 = crypto.randomBytes(32).toString('hex').substring(0, 32);
        const key_iv1 = crypto.randomBytes(16).toString('hex').substring(0, 16);
        const key2 = crypto.randomBytes(32).toString('hex').substring(0, 32);
        const key_iv2 = crypto.randomBytes(16).toString('hex').substring(0, 16);
        const rand_hash = crypto.randomBytes(32).toString('hex').substring(0, 32);
        myJson[RaspberryConstant.randKumuKey] = rand_hash;
        const stringedJson = JSON.stringify(myJson);
        const config = AESEncrypt(stringedJson, key1, key_iv1);
        const jsonHash1 = {
          key: key1,
          iv: key_iv1,
        };
        const jsonHash2 = {
          key: key2,
          iv: key_iv2,
        };
        const stringedJsonHash1 = JSON.stringify(jsonHash1);
        const stringedJsonHash2 = JSON.stringify(jsonHash2);
        const hashedHash1WithHash2 = AESEncrypt(stringedJsonHash1, key2, key_iv2);
        Fs.writeFile(`${dataDirPath}${RaspberryConstant.randKumuKey}`, rand_hash, function (err) {
          if (err) return console.log(err);
        });
        Fs.writeFile(
          `${state.usbPath}/${RaspberryConstant.usbKeyFileName}`,
          hashedHash1WithHash2,
          function (err) {
            if (err) return console.log(err);
          },
        );
        Fs.writeFile(`${dataDirPath}${RaspberryConstant.configFileName}`, config, function (err) {
          if (err) return console.log(err);
        });
        Fs.writeFile(
          `${dataDirPath}${RaspberryConstant.publicKeyFileName}`,
          stringedJsonHash2,
          function (err) {
            if (err) return console.log(err);
          },
        );
        setTimeout(() => {
          this.decripConfigData();
        }, 1000);
      })
      .catch((e) => {
        console.log('ERROR SHOULD REMOVE');
        // this.cleanConfigData();
      });
  };

  cleanConfigData = () => {
    const { wsServer, state } = this;
    Fs.readdir(dataDirPath, (err, dirInfo) => {
      if (err) return { errors: true, err_message: err };
      dirInfo.forEach((pth) => {
        Rimraf(`${dataDirPath}${pth}`, () => {});
        // console.log(`${media}${paths[0]}/${path}/${pth}`);
      });
    });
    Fs.readdir(`${state.usbPath}`, (err, dirInfo) => {
      if (err) return { errors: true, err_message: err };
      dirInfo.forEach((pth) => {
        Rimraf(`${state.usbPath}/${pth}`, () => {});
        // console.log(`${media}${paths[0]}/${path}/${pth}`);
      });
    });
    wsServer.emit(actions.reset_raspberry_frontend);
  };

  decripConfigData = () => {
    const { wsServer, setState, state } = this;
    let publicKey, encriptedUsbKey, usbKey, randKumuKey, encriptedConfig, config;
    console.log("decripConfigData")
    try {
      Fs.readFile(
        `${dataDirPath}${RaspberryConstant.publicKeyFileName}`,
        { encoding: 'utf-8' },
        function (err, data) {
          publicKey = JSON.parse(data.toString());
        },
      );
    } catch (e) {
      console.log(e);
    }
    try {
      Fs.readFile(
        `${dataDirPath}${RaspberryConstant.randKumuKey}`,
        { encoding: 'utf-8' },
        function (err, data) {
          randKumuKey = data.toString();
        },
      );
    } catch (e) {
      console.log(e);
    }
    try {
      Fs.readFile(
        `${dataDirPath}${RaspberryConstant.configFileName}`,
        { encoding: 'utf-8' },
        function (err, data) {
          encriptedConfig = data;
        },
      );
    } catch (e) {
      console.log(e);
    }
    try {
      Fs.readFile(
        `${state.usbPath}/${RaspberryConstant.usbKeyFileName}`,
        { encoding: 'utf-8' },
        function (err, data) {
          encriptedUsbKey = data;
        },
      );
    } catch (e) {
      console.log(e);
    }

    setTimeout(() => {
      try {
        usbKey = AESDecrypt(encriptedUsbKey, publicKey.key, publicKey.iv);
        usbKey = usbKey.toString();
        usbKey = JSON.parse(usbKey);
        config = AESDecrypt(encriptedConfig, usbKey.key, usbKey.iv);
        config = config.toString();
        config = JSON.parse(config);

        console.log("Emiting")

        if (config[RaspberryConstant.randKumuKey] === randKumuKey) {
          setState({
            config,
            serverStarted: true,
          });
          wsServer.emit(actions.config_decrypted);
        } else {
          wsServer.emit(actions.invalid_key);
        }
      } catch (e) {
        console.log(e);
        wsServer.emit(actions.invalid_key);
      }
    }, 1000);
  };

  startCreatingRaspberry = () => {
    const { wsServer } = this;
    const checkPreviousQuery = Fs.existsSync(
      `${dataDirPath}${RaspberryConstant.createRaspberryQuery}`,
    );
    if (!checkPreviousQuery) {
      castSpell(spells.ADD_RASPBERRY_QUERY).then((json) => {
        Fs.writeFile(
          `${dataDirPath}${RaspberryConstant.createRaspberryQuery}`,
          JSON.stringify(json),
          function (err) {
            if (err) return console.log(err);
          },
        );
        console.log('Reading');
        setTimeout(() => {
          try {
            Fs.readFile(
              `${dataDirPath}${RaspberryConstant.createRaspberryQuery}`,
              { encoding: 'utf-8' },
              function (err, data) {
                const json = JSON.parse(data.toString());
                wsServer.emit(actions.qr_code_to_check_raspberry_added_to_house, { data: json });
              },
            );
          } catch (e) {
            console.log(e);
          }
        }, 1000);
      });
    } else {
      try {
        Fs.readFile(
          `${dataDirPath}${RaspberryConstant.createRaspberryQuery}`,
          { encoding: 'utf-8' },
          (err, data) => {
            try {
              const json = JSON.parse(data.toString());
              wsServer.emit(actions.qr_code_to_check_raspberry_added_to_house, { data: json });
            } catch (e) {
              this.cleanConfigData();
            }
          },
        );
      } catch (e) {
        console.log(e);
      }
    }
  };

  handleUsbInserted = () => {
    const { wsServer, emitToWebsocket, setState, state } = this;
    const { serverStart } = state;
    if (!serverStart) {
      emitToWebsocket(actions.usb_connected);
      setTimeout(async () => {
        const drives = await drivelist.list();
        const driveInfo = CheckInsertedUsbPath(drives);
        if (
          driveInfo.data.label !== RaspberryConstant.usbNameConstant &&
          driveInfo.data.label !== 'usb'
        ) {
          await RenameUsbLabelToKumu(driveInfo.data.label);
          emitToWebsocket(actions.usb_reinsert);
          return;
        }
        const infoInUsb = await GetFullPathUsbInfo(driveInfo.data.path);
        setState({
          usbPath: driveInfo.data.path,
        });
        const checkConfigFile = Fs.existsSync(`${dataDirPath}${RaspberryConstant.configFileName}`);
        const checkPublicKeyFile = Fs.existsSync(
          `${dataDirPath}${RaspberryConstant.publicKeyFileName}`,
        );
        wsServer.emit(actions.usb_loaded, { usbHaskey: infoInUsb.usbHasKey });
        if (!infoInUsb.usbHasKey && checkConfigFile && checkPublicKeyFile) {
          emitToWebsocket(actions.invalid_key);
        }
        if (!checkConfigFile || !checkPublicKeyFile) {
          this.startCreatingRaspberry();
        } else {
          this.decripConfigData();
        }
      }, 5000);
    }
  };
}
