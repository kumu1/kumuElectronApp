import { execShellCommand } from '../../../frontend/utils/exec';

const ip_gateway = '192.168.7.10';
const ip_dhcp_start = '192.168.7.11';
const ip_dhcp_stop = '192.168.7.255';

// eslint-disable-next-line no-unused-vars
const StopApNet = async () => {
  await execShellCommand('sudo wifi-ap.config set disabled=true');
  console.log('Execute StopApNet()');
};

// eslint-disable-next-line no-unused-vars
const ConfigApNet = async () => {
  return new Promise(async (resolve) => {
    let response;
    // const net_ssid = this["config"]["ssid_wifi"].toString();
    // const net_password = this["config"]["pass_wifi"].toString();
    response = await execShellCommand('iw dev');
    const InterfacesValues = [];
    if (!response.errors) {
      let Interfaces = response.data.substring(
        response.data.indexOf('Interface'),
        response.data.length,
      );
      Interfaces = Interfaces.split('Interfaces');
      Interfaces.forEach((inter) => {
        const interValue = inter.substring(inter.indexOf(' ') + 1, inter.indexOf(`\n`));
        InterfacesValues.push(interValue);
      });
    }
    let commands = [
      {
        name: `nmcli r wifi on`,
        value: `nmcli r wifi on`,
      },
      {
        name: `sudo wifi-ap.config set wifi.interface`,
        value: `sudo wifi-ap.config set wifi.interface=${InterfacesValues[0]}`,
      },
      {
        name: `sudo wifi-ap.config set wifi.address`,
        value: `sudo wifi-ap.config set wifi.address=${ip_gateway}`,
      },
      {
        name: `sudo wifi-ap.config set wifi.ssid`,
        // value: `sudo wifi-ap.config set wifi.ssid=${net_ssid}`,
        value: `sudo wifi-ap.config set wifi.ssid=${'ssidkumutest'}`,
      },
      {
        name: `sudo wifi-ap.config set wifi.security=wpa2 wifi.security-passphrase`,
        // value: `sudo wifi-ap.config set wifi.security=wpa2 wifi.security-passphrase='${net_password}'`,
        value: `sudo wifi-ap.config set wifi.security=wpa2 wifi.security-passphrase='${'passkumu'}'`,
      },
      {
        name: `sudo wifi-ap.config set share.disabled=false`,
        value: `sudo wifi-ap.config set share.disabled=false`,
      },
      {
        name: `sudo wifi-ap.config set dhcp.range-start`,
        value: `sudo wifi-ap.config set dhcp.range-start=${ip_dhcp_start}`,
      },
      {
        name: `sudo wifi-ap.config set dhcp.range-stop`,
        value: `sudo wifi-ap.config set dhcp.range-stop=${ip_dhcp_stop}`,
      },
      {
        name: `sudo wifi-ap.config set wifi.country-code=AR`,
        value: `sudo wifi-ap.config set wifi.country-code=AR`,
      },
      {
        name: `sudo wifi-ap.config set disabled=false`,
        value: `sudo wifi-ap.config set disabled=false`,
      },
    ];
    for (const cmnd of commands) {
      let response = await execShellCommand(cmnd.value);
      if (!response.errors) {
        console.log(`Command - Success: ${cmnd.name}`);
      } else {
        console.log(`Command - Error: ${cmnd.name}`);
      }
    }
  });
};
