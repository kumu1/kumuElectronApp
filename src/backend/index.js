import Raspberry from './raspberry';

const app = require('express')();
const http = require('http').createServer(app);
const io = require('socket.io')(http);

// app.get('/', (req, res) => {
//   res.sendFile(__dirname + '/index.html');
// });
io.origins('*:*');
new Raspberry({ wsServer: io });
http.listen(3030, () => {
  console.log('listening on *:3030');
});
