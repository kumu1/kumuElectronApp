import fetch from 'node-fetch';

const emiliaConstants = {
  url: 'http://localhost:3001',
};

export const castSpell = (name, data = {}) => {
  return new Promise((resolve, reject) => {
    const spell = {
      spell: name,
      data,
      auth: {},
    };

    fetch(`${emiliaConstants.url}/emilia`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(spell),
    })
      .then(async (response) => {
        if (response.status === 500) {
          try {
            const json = await response.json();
            console.log('Casted Error');
            console.error(json);
            reject(json);
            // process.exit(1);
          } catch (e) {
            console.error(e);
            // process.exit(1);
          }
        }
        console.log('Casted');
        return resolve(response.json());
      })
      .catch((e) => {
        console.log('Casted Error');
        console.error(e);
        reject(e);
        // process.exit(1);
      });
  });
};
