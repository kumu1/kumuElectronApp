export const spells = {
  CREATE_SEEDS: 'CREATE_SEEDS',
  ADD_RASPBERRY_QUERY: 'ADD_RASPBERRY_QUERY',
  ADD_RASPBERRY_TO_RESIDENCE: 'ADD_RASPBERRY_TO_RESIDENCE',
  CHECK_IF_RASPBERRY_IS_ADDED: 'CHECK_IF_RASPBERRY_IS_ADDED',
};
