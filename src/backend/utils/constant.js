export const RaspberryConstant = {
  usbNameConstant: 'KUMUKEY',
  usbKeyFileName: 'kumu_key',
  createRaspberryQuery: 'createRaspberryQuery.json',
  randKumuKey: 'rand_kumu_key',
  publicKeyFileName: 'p_kumu_key',
  configFileName: 'config',
};
export const FunctionsServer = {
  // host: 'http://localhost:5001/kumu-32a79/us-central1/',
  host: 'http://192.168.1.196:5001/kumu-32a79/us-central1/',
  check_if_raspberry_was_added: 'check_if_raspberry_was_added',
  create_raspberry_query: 'create_raspberry_query',
};
