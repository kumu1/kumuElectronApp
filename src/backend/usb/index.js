import Fs from 'fs';
import { RaspberryConstant } from '../utils/constant';
import Rimraf from 'rimraf';
import { execShellCommand } from '../../frontend/utils/exec';

export const GetFullPathUsbInfo = (path) => {
  return new Promise(async (resolve, reject) => {
    Fs.readdir(`${path}`, (err, infoInUsb) => {
      if (err) return reject({ err_message: err });
      const kumuKeyIndex = infoInUsb.findIndex((elem) => elem === RaspberryConstant.usbKeyFileName);
      if (kumuKeyIndex === -1) {
        return resolve({
          usbHasKey: false,
        });
      } else {
        infoInUsb.forEach((pth) => {
          if (
            pth !== RaspberryConstant.usbKeyFileName &&
            pth !== '.Trash-1000' &&
            pth !== 'System Volume Information'
          ) {
            Rimraf(`${path}/${pth}`, () => {});
            // console.log(`${media}${paths[0]}/${path}/${pth}`);
          }
        });
        return resolve({
          usbHasKey: true,
        });
      }
    });
  });
};

export const CheckInsertedUsbPath = (drives) => {
  const drive = drives.find((elem) => elem.busType === 'USB');
  if (drive !== -1) {
    const driveInfo = {
      device: drive.device,
      path: drive.mountpoints[0].path,
      label: drive.mountpoints[0].label,
    };
    return { errors: false, data: driveInfo };
  } else {
    console.log('No hay usb');
    return { errors: true };
  }
};
export const RenameUsbLabelToKumu = (labelReference) => {
  return new Promise(async (resolve, reject) => {
    let response = await execShellCommand('sudo blkid');
    const responseSplit = response.data.split('\n');
    let maybeUsb = [];
    responseSplit.forEach((elem) => {
      if (elem.indexOf(labelReference) !== -1) {
        maybeUsb.push(elem);
      }
    });
    if (maybeUsb.length > 0) {
      const whereUsbRow = maybeUsb[maybeUsb.length - 1];
      if (whereUsbRow !== -1) {
        const doubleDot = whereUsbRow.indexOf(':');
        const whereUsbPoint = whereUsbRow.substring(0, doubleDot);
        await execShellCommand(`sudo umount ${whereUsbPoint}`);
        await execShellCommand(
          `sudo mlabel -i ${whereUsbPoint} -s ::"${RaspberryConstant.usbNameConstant}"`,
        );
        resolve();
      }
    }
    reject();
  });
};
