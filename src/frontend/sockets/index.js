import { USB_DeclareSockets } from './usb';

export const DeclareSockets = ({ wsServer, dispatch }) => {
  USB_DeclareSockets({ wsServer, dispatch });
};
