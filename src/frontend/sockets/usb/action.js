const actions = {
  usb_connected: 'usb_connected',
  usb_list: 'usb_list',
  is_there_saved_data: 'is_there_saved_data',
  usb_reinsert: 'usb_reinsert',
  usb_loaded: 'usb_loaded',
  invalid_key: 'invalid_key',
  qr_code_to_check_raspberry_added_to_house: 'qr_code_to_check_raspberry_added_to_house',
  check_qr_code_raspberry: 'check_qr_code_raspberry',
  reset_raspberry_frontend: 'reset_raspberry_frontend',
  reset_raspberry_backend: 'reset_raspberry_backend',
};
export default actions;
