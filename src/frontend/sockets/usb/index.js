import UsbSocketActions from './action';
import {
  CheckIsSavedData,
  SetQrHash,
  StartLoading,
  UsbInserted,
  UsbInsertedAndLoaded,
} from '../../redux/general/action';
import { strings } from '../../utils/strings';
export const USB_DeclareSockets = ({ wsServer, dispatch }) => {
  wsServer.on(UsbSocketActions.usb_connected, (json) => {
    console.log('usb_connected');
    dispatch(UsbInserted(true));
  });
  wsServer.on(UsbSocketActions.usb_reinsert, (json) => {
    console.log('usb_reinsert');
    alert(strings.UsbCol_COMPONENT.alert1);
    dispatch(UsbInserted(false));
  });
  wsServer.on(UsbSocketActions.usb_loaded, (json) => {
    console.log('usb_loaded');
    dispatch(StartLoading());
    dispatch(UsbInsertedAndLoaded(true));
  });
  wsServer.on(UsbSocketActions.is_there_saved_data, (json) => {
    console.log('is_there_saved_data');
    console.log(json);
    dispatch(CheckIsSavedData(json.data));
  });
  wsServer.on(UsbSocketActions.invalid_key, (json) => {
    console.log('invalid_key');
    alert(strings.UsbCol_COMPONENT.alert2);
    dispatch(UsbInserted(false));
  });
  wsServer.on(UsbSocketActions.qr_code_to_check_raspberry_added_to_house, (json) => {
    console.log('qr_code_to_check_raspberry_added_to_house');
    console.log(json);
    dispatch(SetQrHash(json.data.id));
  });
  wsServer.on(UsbSocketActions.reset_raspberry_frontend, (json) => {
    console.log('reset_raspberry_frontend');
    console.log(json);
    window.location.reload();
  });
};
export const CheckQrCodeRaspberry = ({ socket, qrCode }) => {
  socket.emit(UsbSocketActions.check_qr_code_raspberry, { data: qrCode });
};
export const ResetRaspberryBackend = ({ socket }) => {
  socket.emit(UsbSocketActions.reset_raspberry_backend);
};
