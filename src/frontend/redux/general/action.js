import {
  IS_THERE_USB_DATA,
  SET_LOADING,
  SET_QR_HASH,
  SET_SOCKET,
  SET_VIEW,
  TEST_INIT,
  USB_HAS_KEY,
  USB_INSERTED,
} from './actionTypes';
import { NAVIGATOR_VIEW_CONSTANTS } from '../../navigator/constants';
import { GeneralReducerName } from './index';
import { CheckQrCodeRaspberry } from '../../sockets/usb';

export const TestAction = (view) => async (dispatch) => {
  console.log('Test Action');
  dispatch({ type: TEST_INIT, payload: view });
  return Promise.resolve();
};
export const StartLoading = () => async (dispatch) => {
  dispatch({ type: SET_LOADING, payload: true });
  return Promise.resolve();
};
export const StopLoading = () => async (dispatch) => {
  dispatch({ type: SET_LOADING, payload: false });
  return Promise.resolve();
};
export const UsbInserted = (payload) => async (dispatch) => {
  dispatch({ type: USB_INSERTED, payload });
  return Promise.resolve();
};
export const UsbInsertedAndLoaded = (payload) => async (dispatch) => {
  dispatch({ type: USB_HAS_KEY, payload });
  return Promise.resolve();
};
export const CheckIsSavedData = (payload) => async (dispatch) => {
  dispatch({ type: IS_THERE_USB_DATA, payload });
  return Promise.resolve();
};
export const SetView = (payload) => async (dispatch) => {
  dispatch({ type: SET_VIEW, payload });
  return Promise.resolve();
};
export const SetQrHash = (payload) => async (dispatch) => {
  dispatch({ type: SET_QR_HASH, payload });
  dispatch({ type: SET_VIEW, payload: NAVIGATOR_VIEW_CONSTANTS.QR_INIT_CONFIG_VIEW });
  dispatch(StopLoading());
  return Promise.resolve();
};
export const SetSocket = (socket) => async (dispatch) => {
  dispatch({ type: SET_SOCKET, payload: socket });
};
export const CheckResetQrCodeRaspberry = () => async (dispatch, getState) => {
  const state = getState()[GeneralReducerName];
  const { socket, qrHash } = state;
  CheckQrCodeRaspberry({ socket, qrCode: qrHash });
};
export const ActionResetRaspberryBackend = () => async (dispatch, getState) => {
  const state = getState()[GeneralReducerName];
  const { socket } = state;
  CheckQrCodeRaspberry({ socket });
};
