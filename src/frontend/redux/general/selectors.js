import NAME from './constants';

export const requestTestState = (store) => store[NAME].testState;
