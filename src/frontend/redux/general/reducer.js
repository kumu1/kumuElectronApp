import {
  IS_THERE_USB_DATA,
  SET_LOADING,
  SET_QR_HASH,
  SET_SOCKET,
  SET_VIEW,
  TEST_INIT,
  USB_HAS_KEY,
  USB_INSERTED,
} from './actionTypes';
import { NAVIGATOR_VIEW_CONSTANTS } from '../../navigator/constants';

const initialState = {
  loading: false,
  usbConnectedLoading: false,
  usbHasKey: false,
  isThereSavedData: false,
  view: NAVIGATOR_VIEW_CONSTANTS.INSERT_USB_VIEW,
  qrHash: '',
  socket: {},
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case TEST_INIT:
      return { ...state, testState: true };
    case SET_LOADING:
      return { ...state, loading: action.payload };
    case IS_THERE_USB_DATA:
      return { ...state, loading: false, isThereSavedData: action.payload };
    case USB_INSERTED:
      return { ...state, usbConnectedLoading: action.payload };
    case SET_VIEW:
      return { ...state, view: action.payload };
    case SET_QR_HASH:
      return { ...state, qrHash: action.payload };
    case SET_SOCKET:
      return { ...state, socket: action.payload };
    case USB_HAS_KEY:
      return { ...state, usbConnectedLoading: false, usbHasKey: action.payload };
    default:
      return state;
  }
}
