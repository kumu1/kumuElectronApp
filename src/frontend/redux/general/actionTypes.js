import NAME from './constants';

export const TEST_INIT = NAME + '/TEST_INIT';
export const IS_THERE_USB_DATA = NAME + '/IS_THERE_USB_DATA';
export const USB_INSERTED = NAME + '/USB_INSERTED';
export const USB_HAS_KEY = NAME + '/USB_HAS_KEY';
export const SET_VIEW = NAME + '/SET_VIEW';
export const SET_LOADING = NAME + '/SET_LOADING';
export const SET_QR_HASH = NAME + '/SET_QR_HASH';
export const SET_SOCKET = NAME + '/SET_SOCKET';
