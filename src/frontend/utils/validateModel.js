import { validate } from 'bycontract';

export function validateModel(data, values) {
  validate(data, 'array');
  let toValidateData = {};
  data.forEach((dta) => {
    if (dta.required) {
      toValidateData[dta.name] = dta.type;
    }
  });
  validate(values, { ...toValidateData });
}
