export const colors = {
  black: '#000',
  white: '#fff',
  purple: '#9d22ba',
  'black-grey': '#505050',
  'white-grey': '#f5f5f5',
};
