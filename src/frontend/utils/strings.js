export const strings = {
  LOGIN_VIEW: {
    title: 'Iniciar Sesión',
    email: 'Email',
    password: 'Contraseña',
    'forgot-register': 'Olvide mi contraseña / registrarme',
    googleSingIn: 'Conectarse con Google',
    button: 'Ingresar',
  },
  WifiNetLeftCol_COMPONENT: {
    text1: 'Configuracion Primaria',
    text2: 'Red Wifi',
  },
  WifiNetRightCol_COMPONENT: {
    conectTo: 'Conectarse a:',
    notWifiDetected1: 'No hemos detectado ninguna red wifi en las proximidades.',
    notWifiDetected2:
      '¿Tenes la encendida la placa de red? Presiona F11 y intenta encender las redes wifi',
    notWifiDetected3:
      'Si sabes lo que haces, recomendamos configurar la red wifi de manera manual, esto es Raspian',
    notWifiDetected4: '(Raspberry Pi OS es una distribución del sistema operativo GNU/Linux)',
    again: 'Reintentar',
    skip: 'Skip',
  },
  InsertWifiPassRightCol_COMPONENT: {
    title: 'Insertar contraseña - Conectarse a:',
    pass: 'Contraseña',
    connect: 'Conectarse',
    skip: 'Skip',
  },
  QrCol_COMPONENT: {
    title: 'Escanea el codigo qr con la opcion ',
    title2: 'Agregar M-Box',
    check_reset: 'Checkear / Resetear',
  },
  UsbCol_COMPONENT: {
    title: 'Insertar la llave USB',
    addNew: 'Resetear / Agregar nueva',
    alert1: 'Este usb se ha configurado correctamente, reinserta el usb para continuar.',
    alert2:
      'Esta llave usb es invalida para esta box configurada. Insertar la correcta o reconfigurar la box',
  },
};
