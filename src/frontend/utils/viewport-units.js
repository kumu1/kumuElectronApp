const actualWidth = window.innerWidth;
const actualPerWith = window.innerWidth / 100;
const actualPerHeight = window.innerHeight / 100;
const vw = (value) => {
  return (value * actualPerWith).toFixed(2);
};
const vh = (value) => {
  return parseFloat(value * actualPerHeight).toFixed(2);
};
const ResponsiveSize = (size) => {
  const baseSizeWidth = 360;
  return parseFloat((actualWidth * size) / baseSizeWidth).toFixed(2);
};

export { vw, vh, ResponsiveSize };
