export function execShellCommand(cmd) {
  const exec = require('child_process').exec;
  return new Promise((resolve, reject) => {
    exec(cmd, (error, stdout, stderr) => {
      if (error) {
        console.warn(error);
        reject();
        console.error('Error in command: ', cmd);
        process.exit();
      }
      if (!stderr) {
        resolve({
          data: stdout,
        });
      } else {
        console.error('Error in command: ', cmd);
        console.error(stderr);
        process.exit();
        reject({
          data: stderr,
        });
      }
    });
  });
}
