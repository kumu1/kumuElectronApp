import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { WifiNetLeftCol } from '../../components/wifiNets/leftCol';
import { WifiNetRightCol } from '../../components/wifiNets/rightCol';
import { InsertWifiPassRightCol } from '../../components/wifiNets/rightColInsertPass';
import { NAVIGATOR_VIEW_CONSTANTS } from '../../navigator/constants';
import { SetView } from '../../redux/general/action';

const Container = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
`;

class WifiNetContainer extends React.Component {
  state = {
    wifiNets: [],
    loaded: false,
    netSelected: false,
    netConnecting: false,
    // netSelected: {}
  };
  ScanWifiNets = () => {
    this.setState({
      wifiNets: [],
      loaded: false,
    });
    // ipcRenderer.on(WifiNetActions.connectToWifi, (event, data) => {
    //   console.log("RECIEVING CONNECTO WIFI")
    //   if(!data.errors){
    //     alert('TODO OK')
    //   } else {
    //     alert('Error')
    //     console.log(data.error_message)
    //   }
    //   this.setState({
    //     netConnecting: false
    //   })
    // })
    // fetch(
    //   `${window.location.protocol}//${window.location.hostname}${SERVER_PORT}${WifiNetActions.scanWifi}`
    // );
    // .then(function (response) {
    //   return response.json();
    // })
    // .then((myJson) => {
    //   console.log(myJson);
    //   let wifiNets = [];
    //   let wifiNetsKeys = [];
    //   if (!myJson.errors) {
    //     myJson.data.map((dta) => {
    //       if (wifiNetsKeys.indexOf(dta.ssid) === -1) {
    //         wifiNetsKeys.push(dta.ssid);
    //         wifiNets.push(dta);
    //       }
    //     });
    //   }
    //   this.setState({
    //     wifiNets,
    //     loaded: true,
    //   });
    // });
  };
  handleNetSelected = (net) => {
    this.setState({
      netSelected: net,
    });
  };
  handleBackPress = () => {
    this.setState({
      netSelected: false,
    });
  };
  componentDidMount() {
    this.ScanWifiNets();
  }
  skipPressed = () => {
    const { dispatch } = this.props;
    dispatch(SetView(NAVIGATOR_VIEW_CONSTANTS.QR_INIT_CONFIG_VIEW));
  };
  connectToWifi = (pass) => {
    // const { netSelected } = this.state;
    // this.setState({
    //   netConnecting: true,
    // });
    // let url = `${window.location.protocol}//${window.location.hostname}${SERVER_PORT}${WifiNetActions.connectToWifi}`;
    // url += `?ssid=${netSelected.ssid}`;
    // url += `&pass=${pass}`;
    // fetch(url)
    //   .then(function (response) {
    //     return response.json();
    //   })
    //   .then((myJson) => {
    //     console.log("RECIEVING CONNECTO WIFI");
    //     if (!myJson.errors) {
    //       alert("TODO OK");
    //     } else {
    //       alert("Error");
    //       console.log(myJson.error_message);
    //     }
    //     this.setState({
    //       netConnecting: false,
    //     });
    //   });
  };

  render() {
    const { state } = this;
    return (
      <Container>
        <WifiNetLeftCol />
        {!state.netSelected ? (
          <WifiNetRightCol
            handleNetSelected={this.handleNetSelected}
            ScanWifiNets={this.ScanWifiNets}
            loaded={state.loaded}
            wifiNets={state.wifiNets}
            skipPressed={this.skipPressed}
          />
        ) : (
          <InsertWifiPassRightCol
            backPress={this.handleBackPress}
            net={state.netSelected}
            handleConnectToWifiPress={this.connectToWifi}
            netConnecting={state.netConnecting}
            skipPressed={this.skipPressed}
          />
        )}
      </Container>
    );
  }
}
export default connect()(WifiNetContainer);
