import React from 'react';
import styled from 'styled-components';
import { LeftCol } from '../../components/qr/leftCol';
import { UsbCol } from '../../components/usb/usbCol';
import { connect } from 'react-redux';
import { GeneralReducerName } from '../../redux/general';

const Container = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
`;

class UsbContainer extends React.Component {
  render() {
    const { props } = this;
    return (
      <Container>
        <LeftCol />
        <UsbCol usbConnectedLoading={props.usbConnectedLoading} />
      </Container>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    ...{
      usbConnectedLoading: state[GeneralReducerName].usbConnectedLoading,
    },
  };
};
export default connect(mapStateToProps)(UsbContainer);
