import React from 'react';
import styled from 'styled-components';
import { LoginLeftCol } from '../../components/login/leftCol';
import { LoginRightCol } from '../../components/login/rightCol';

const Container = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
`;

class LoginContainer extends React.Component {
  render() {
    return (
      <Container>
        <LoginLeftCol />
        <LoginRightCol />
      </Container>
    );
  }
}
export default LoginContainer;
