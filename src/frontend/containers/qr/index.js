import React from 'react';
import styled from 'styled-components';
import { LeftCol } from '../../components/qr/leftCol';
import { QrCol } from '../../components/qr/qrCol';

const Container = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
`;

class QrContainer extends React.Component {
  render() {
    return (
      <Container>
        <LeftCol />
        <QrCol />
      </Container>
    );
  }
}
export default QrContainer;
