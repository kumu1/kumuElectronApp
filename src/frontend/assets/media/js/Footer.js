import * as React from 'react';

function SvgFooter(props) {
  return (
    <svg viewBox="12 11 360 59" {...props}>
      <defs>
        <filter id="footer_svg__a" x={0} y={0} filterUnits="userSpaceOnUse">
          <feOffset dy={1} />
          <feGaussianBlur stdDeviation={4} result="blur" />
          <feFlood floodOpacity={0.761} />
          <feComposite operator="in" in2="blur" />
          <feComposite in="SourceGraphic" />
        </filter>
      </defs>
      <g filter="url(#footer_svg__a)">
        <path
          data-name="Subtraction 2"
          d="M372 70H12V11h152.243A29.688 29.688 0 00192.5 32a29.688 29.688 0 0028.256-21H372v59z"
          fill="#9d22ba"
        />
      </g>
    </svg>
  );
}

export default SvgFooter;
