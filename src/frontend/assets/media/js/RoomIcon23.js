import * as React from 'react';

function SvgRoomIcon23(props) {
  return (
    <svg width={512} height={512} {...props}>
      <g data-name="Filled outline" transform="matrix(-1 0 0 1 512 0)">
        <path
          d="M344 488H168v-88h-24A120 120 0 0124 280v-24h464v32a8 8 0 01-8 8H344zM360 24h112a16 16 0 0116 16v216H344V40a16 16 0 0116-16z"
          data-original="#348ED8"
          fill="#348ed8"
        />
        <rect
          x={24}
          y={224}
          width={272}
          height={32}
          rx={16}
          ry={16}
          data-original="#D5D5D3"
          fill="#d5d5d3"
        />
        <rect
          x={164}
          y={124}
          width={232}
          height={32}
          rx={16}
          ry={16}
          transform="rotate(90 280 140)"
          data-original="#DFDFDD"
          fill="#dfdfdd"
        />
        <path
          d="M440 144h-56v-16l24-8v-16a8 8 0 018-8h24a8 8 0 018 8v32a8 8 0 01-8 8z"
          data-original="#DFDFDD"
          fill="#dfdfdd"
        />
        <path
          d="M336 480H176v-72h32v-16h-64a112.274 112.274 0 01-107.975-82.128L20.6 314.128A128.318 128.318 0 00144 408h16v80a8 8 0 008 8h176a8 8 0 008-8v-48h-16zM48.032 288.721a95.661 95.661 0 003.237 17.464l15.392-4.37a79.542 79.542 0 01-2.693-14.536z"
          data-original="#000000"
          className="room-icon-23_svg__active-path"
        />
        <path
          d="M126.549 374.41l2.9-15.734a80.613 80.613 0 01-56.706-42.324l-14.238 7.3a96.708 96.708 0 0068.044 50.758z"
          data-original="#000000"
          className="room-icon-23_svg__active-path"
        />
        <path
          d="M472 16H360a24.028 24.028 0 00-24 24v208h-33.361a45.008 45.008 0 001.237-5.554C303.911 242.1 304 40 304 40a24 24 0 00-48 0v176H40a24.019 24.019 0 00-21.664 34.352A7.974 7.974 0 0016 256v24a129.534 129.534 0 001.128 17.057l15.859-2.114A113.622 113.622 0 0132 280v-16h448v24H344a8 8 0 00-8 8v128h16V304h128a16.019 16.019 0 0016-16V40a24.028 24.028 0 00-24-24zM360 32h112a8.009 8.009 0 018 8v16H352V40a8.009 8.009 0 018-8zm-88 8a8 8 0 0116 0v200a8.071 8.071 0 01-.041.812c0 .029-.01.058-.013.087a8.011 8.011 0 01-7.033 7.045c-.034 0-.067.012-.1.015A8 8 0 01272 240zM34.347 245.674A8.009 8.009 0 0140 232h216v8a23.889 23.889 0 001.376 8H40a7.974 7.974 0 01-5.653-2.326zM352 248V72h128v176z"
          data-original="#000000"
          className="room-icon-23_svg__active-path"
        />
        <path
          d="M440 88h-24a16.019 16.019 0 00-16 16v10.234l-18.53 6.176A8 8 0 00376 128v16a8 8 0 008 8h56a16.019 16.019 0 0016-16v-32a16.019 16.019 0 00-16-16zm0 48h-48v-2.234l18.53-6.176A8 8 0 00416 120v-16h24l.01 32z"
          data-original="#000000"
          className="room-icon-23_svg__active-path"
        />
      </g>
    </svg>
  );
}

export default SvgRoomIcon23;
