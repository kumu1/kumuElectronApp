import * as React from 'react';

function SvgFlashOn(props) {
  return (
    <svg viewBox="0 0 512 512" {...props}>
      <path d="M449.122 161.803H273.683L366.935 0H156.027L62.878 266.202h149.75L146.249 512z" />
    </svg>
  );
}

export default SvgFlashOn;
