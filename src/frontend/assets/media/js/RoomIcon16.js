import * as React from 'react';

function SvgRoomIcon16(props) {
  return (
    <svg height={512} viewBox="0 0 64 64" width={512} {...props}>
      <g data-name="Layer 15">
        <path d="M12 42h40v18H12z" fill="#c18e5c" />
        <path d="M2 36h60v6H2zM52 42h6v20h-6zM6 42h6v20H6z" fill="#cda57d" />
        <path d="M38 32h22v4H38z" fill="#a0a8b2" />
        <path d="M40 32V20a2 2 0 012-2h14a2 2 0 012 2v12zM26 26h8v10h-8z" fill="#c1c8d1" />
        <path d="M4 32h10v4H4z" fill="#f7d881" />
        <path d="M2 28h10v4H2z" fill="#4472b0" />
        <circle cx={49} cy={25} fill="#ebf7fe" r={3} />
        <path d="M26 33h-2a3 3 0 010-6h2v2h-2a1 1 0 000 2h2z" fill="#a0a8b2" />
        <path
          d="M27.71 23.71l-1.42-1.42 1.3-1.29a1.42 1.42 0 000-2l-.59-.59A3.36 3.36 0 0126 16v-1h2v1a1.43 1.43 0 00.41 1l.59.59a3.4 3.4 0 010 4.82z"
          fill="#c1c8d1"
        />
        <path
          d="M31.71 23.71l-1.42-1.42 1.3-1.29a1.42 1.42 0 000-2l-.59-.59A3.36 3.36 0 0130 16v-1h2v1a1.43 1.43 0 00.41 1l.59.59a3.4 3.4 0 010 4.82z"
          fill="#c1c8d1"
        />
        <circle cx={12} cy={12} fill="#c1c8d1" r={10} />
        <g fill="#ebf7fe">
          <path d="M13.05 2.06A9.24 9.24 0 0012 2h-.95L11 5h2zM11 22c.33 0 .66.05 1 .05s.67 0 1-.05v-3h-2zM2 12a9.08 9.08 0 000 .91L5 13v-2l-3-.09A9.82 9.82 0 002 12zM19 13h3c0-.33.05-.66.05-1s0-.67-.05-1h-3zM18.33 4.26l-2 2 1.42 1.42 2-2a10.44 10.44 0 00-1.42-1.42zM4.26 18.33a10.44 10.44 0 001.41 1.41l2-2-1.38-1.45zM4.26 5.67l2 2 1.45-1.38-2-2a10.44 10.44 0 00-1.45 1.38zM17.71 16.29l-1.42 1.42 2 2a10.44 10.44 0 001.41-1.41zM15.29 16.71l-4-4A1 1 0 0111 12V7h2v4.59l3.71 3.7z" />
        </g>
      </g>
    </svg>
  );
}

export default SvgRoomIcon16;
