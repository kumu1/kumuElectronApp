import * as React from 'react';

function SvgThermometer(props) {
  return (
    <svg
      id="thermometer_svg__Capa_1"
      height={512}
      viewBox="0 0 496.648 496.648"
      width={512}
      {...props}
    >
      <link type="text/css" rel="stylesheet" id="thermometer_svg__dark-mode-custom-link" />
      <style type="text/css" id="dark-mode-custom-style" />
      <path
        d="M303.2 245.953V55.376C303.2 24.793 278.408 0 247.825 0h-.001c-30.583 0-55.376 24.793-55.376 55.376V246.42c-45.962 21.64-77.347 69.139-75.378 123.746 2.523 69.951 60.31 125.986 130.305 126.48 72.973.515 132.29-58.484 132.29-131.338.001-52.948-31.333-98.57-76.465-119.355z"
        data-original="#FAF7F7"
        data-old_color="#faf7f7"
      />
      <path
        d="M294.217 483.073c-76.734-.542-140.085-61.971-142.851-138.657-1.232-34.149 9.453-65.763 28.197-91.025-38.821 23.905-64.272 67.441-62.493 116.774 2.523 69.951 60.311 125.986 130.305 126.48 21.678.153 42.15-4.946 60.221-14.101-4.407.375-8.868.561-13.379.529z"
        data-original="#F9F3F1"
        data-old_color="#f9f3f1"
      />
      <path
        d="M290.654 273.202c35.85 16.51 59.01 52.66 59.01 92.1 0 55.88-45.46 101.35-101.34 101.35s-101.34-45.47-101.34-101.35c0-39.07 22.86-75.08 58.24-91.74l12.97-6.11a7.392 7.392 0 004.25-6.7V55.372c0-14.01 11.37-25.37 25.38-25.37 7 0 13.35 2.84 17.94 7.43 4.6 4.59 7.44 10.94 7.44 17.94v205.05c0 2.89 1.68 5.52 4.3 6.73z"
        fill="#f9b1b1"
        data-original="#F9B1B1"
        id="thermometer_svg__XMLID_213_"
      />
      <path
        d="M273.028 355.356l1.349-10.269a26.178 26.178 0 00-5.408-19.63l-.069-.087c-7.356-9.316-14.369-17.099-18.161-21.185a3.972 3.972 0 00-5.829-.002c-10.32 11.12-44.519 49.648-44.519 71.892 0 26.196 21.236 47.432 47.432 47.432s47.432-21.236 47.432-47.432c0-5.763-2.297-12.62-5.879-19.81a1.817 1.817 0 00-2.387-.828l-8.515 3.93c-2.76 1.273-5.843-.996-5.446-4.011z"
        fill="#c12020"
        data-original="#F37C7C"
        className="thermometer_svg__active-path"
        data-old_color="#f37c7c"
      />
      <g fill="#f79595">
        <path
          d="M273.204 216.976v-14.999h-50.76v14.999l.004.001H273.2zM273.204 133.049v-15h-50.76v15H273.2z"
          data-original="#F79595"
        />
      </g>
    </svg>
  );
}

export default SvgThermometer;
