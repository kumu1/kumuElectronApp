import * as React from 'react';

function SvgRoomIcon7(props) {
  return (
    <svg height={512} width={512} {...props}>
      <path d="M104 24h304v32H104z" fill="#68737f" />
      <path d="M128 56h256v168H128z" fill="#d4e4f0" />
      <path d="M152 256h208v152H152z" fill="#ba8b5d" />
      <path d="M120 336H24V232h32v56h64z" fill="#529ddf" />
      <path d="M56 288h16v24H56z" fill="#4384c3" />
      <path d="M64 336h16v32H64z" fill="#4f5761" />
      <path
        d="M96 392a8 8 0 01-8-8v-8H56v8a8 8 0 01-16 0v-16a8 8 0 018-8h48a8 8 0 018 8v16a8 8 0 01-8 8z"
        fill="#68737f"
      />
      <path d="M24 320h96v16H24z" fill="#4384c3" />
      <path d="M152 408h32v80h-32zM328 408h32v80h-32z" fill="#9f764a" />
      <path
        d="M207.378 368.645L187.056 328H176v-16h16a8 8 0 017.155 4.422l22.533 45.066z"
        fill="#4f5761"
      />
      <path d="M184 352h48v32h-48z" fill="#68737f" />
      <path
        d="M208.045 273.979L187.055 232H176v-16h16a8 8 0 017.155 4.422l23.2 46.4z"
        fill="#4f5761"
      />
      <path d="M184 256h48v32h-48z" fill="#68737f" />
      <path
        d="M304.622 368.645l-14.31-7.157 22.533-45.066A8 8 0 01320 312h16v16h-11.056z"
        fill="#4f5761"
      />
      <path d="M328 384h-48v-32h48z" fill="#68737f" />
      <path d="M303.155 275.578l-14.31-7.156 24-48A8 8 0 01320 216h16v16h-11.056z" fill="#4f5761" />
      <path d="M328 288h-48v-32h48z" fill="#68737f" />
      <path d="M120 440H24V336h32v56h64z" fill="#529ddf" />
      <path d="M56 392h16v24H56z" fill="#4384c3" />
      <path d="M64 440h16v32H64z" fill="#4f5761" />
      <path
        d="M96 496a8 8 0 01-8-8v-8H56v8a8 8 0 01-16 0v-16a8 8 0 018-8h48a8 8 0 018 8v16a8 8 0 01-8 8z"
        fill="#68737f"
      />
      <path d="M24 424h96v16H24z" fill="#4384c3" />
      <path d="M392 336h96V232h-32v56h-64z" fill="#529ddf" />
      <path d="M440 288h16v24h-16z" fill="#4384c3" />
      <path d="M432 336h16v32h-16z" fill="#4f5761" />
      <path
        d="M416 392a8 8 0 008-8v-8h32v8a8 8 0 0016 0v-16a8 8 0 00-8-8h-48a8 8 0 00-8 8v16a8 8 0 008 8z"
        fill="#68737f"
      />
      <path d="M488 336h-96v-16h96z" fill="#4384c3" />
      <path d="M392 440h96V336h-32v56h-64z" fill="#529ddf" />
      <path d="M440 392h16v24h-16z" fill="#4384c3" />
      <path d="M432 440h16v32h-16z" fill="#4f5761" />
      <path
        d="M416 496a8 8 0 008-8v-8h32v8a8 8 0 0016 0v-16a8 8 0 00-8-8h-48a8 8 0 00-8 8v16a8 8 0 008 8z"
        fill="#68737f"
      />
      <path d="M488 440h-96v-16h96z" fill="#4384c3" />
    </svg>
  );
}

export default SvgRoomIcon7;
