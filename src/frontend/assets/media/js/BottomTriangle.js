import * as React from 'react';

function SvgBottomTriangle(props) {
  return (
    <svg
      id="bottom-triangle_svg__Layer_1"
      x={0}
      y={0}
      xmlSpace="preserve"
      width={512}
      height={512}
      {...props}
    >
      <g transform="matrix(1 0 0 -1 0 512)">
        <link
          type="text/css"
          rel="stylesheet"
          id="bottom-triangle_svg__dark-mode-custom-link"
          className="bottom-triangle_svg__active-path"
        />
        <style type="text/css" id="dark-mode-custom-style" />
        <path
          d="M507.521 427.394L282.655 52.617c-12.074-20.122-41.237-20.122-53.311 0L4.479 427.394c-12.433 20.72 2.493 47.08 26.655 47.08h449.732c24.163 0 39.089-26.36 26.655-47.08z"
          data-original="#000000"
          className="bottom-triangle_svg__active-path"
        />
      </g>
    </svg>
  );
}

export default SvgBottomTriangle;
