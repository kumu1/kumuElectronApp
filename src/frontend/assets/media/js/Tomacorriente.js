import * as React from 'react';

function SvgTomacorriente(props) {
  return (
    <svg width={512} height={512} {...props}>
      <path
        d="M437 0H256L128 256l128 256h181c41.355 0 75-33.645 75-75V75c0-41.355-33.645-75-75-75z"
        fill="#bddbe0"
      />
      <path
        d="M75 0C33.645 0 0 33.645 0 75v362c0 41.355 33.645 75 75 75h181V0H75z"
        fill="#d6faff"
      />
      <path fill="#d6faff" d="M421 90H256l-60 166 60 166h165z" />
      <path fill="#fff" d="M91 90h165v332H91z" />
      <path fill="#41494e" d="M166 165h30v91h-30z" />
      <path
        fill="#292c32"
        d="M316 165h30v91h-30zM301.5 301.5c0-25.089-20.411-45.5-45.5-45.5l-30 45.5 30 45.5h45.5v-45.5z"
      />
      <path d="M210.5 301.5V347H256v-91c-25.089 0-45.5 20.411-45.5 45.5z" fill="#41494e" />
    </svg>
  );
}

export default SvgTomacorriente;
