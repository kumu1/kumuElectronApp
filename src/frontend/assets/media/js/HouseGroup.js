import * as React from 'react';

function SvgHouseGroup(props) {
  return (
    <svg data-name="Component 2 \u2013 1" width={34.31} height={17.651} {...props}>
      <g data-name="Group 3">
        <g data-name="Group 2">
          <path
            data-name="Path 1"
            d="M34.086 5.625L28.114.18a.679.679 0 00-.919 0l-5.972 5.445a.682.682 0 00.46 1.187h.954v5.452a.391.391 0 00.391.391H26.3a.391.391 0 00.391-.391v-3.31h1.926v3.31a.391.391 0 00.391.391h3.273a.391.391 0 00.391-.391V6.808h.954a.682.682 0 00.46-1.187z"
            fill="#9d22ba"
          />
        </g>
      </g>
      <g data-name="Group 5">
        <g data-name="Group 4">
          <path
            data-name="Path 2"
            d="M32.569.782H29.94l3.02 2.746V1.173a.391.391 0 00-.391-.391z"
            fill="#9d22ba"
          />
        </g>
      </g>
      <g data-name="logo">
        <g data-name="Group 3">
          <g data-name="Group 2">
            <path
              data-name="Path 1"
              d="M13.086 5.625L7.114.18a.679.679 0 00-.919 0L.223 5.625a.682.682 0 00.46 1.187h.954v5.452a.391.391 0 00.391.391H5.3a.391.391 0 00.391-.391v-3.31h1.926v3.31a.391.391 0 00.391.391h3.273a.391.391 0 00.391-.391V6.808h.954a.682.682 0 00.46-1.187z"
              fill="#9d22ba"
            />
          </g>
        </g>
        <g data-name="Group 5">
          <g data-name="Group 4">
            <path
              data-name="Path 2"
              d="M11.569.782H8.94l3.02 2.746V1.173a.391.391 0 00-.391-.391z"
              fill="#9d22ba"
            />
          </g>
        </g>
      </g>
      <g data-name="logo">
        <g data-name="Group 3">
          <g data-name="Group 2">
            <path
              data-name="Path 1"
              d="M25.358 8.325l-7.921-7.218a.9.9 0 00-1.22 0L8.3 8.325a.905.905 0 00.61 1.574h1.261v7.232a.519.519 0 00.519.519h4.342a.519.519 0 00.519-.519V12.74H18.1v4.391a.519.519 0 00.519.519h4.342a.519.519 0 00.519-.519V9.899h1.265a.905.905 0 00.61-1.575z"
            />
          </g>
        </g>
        <g data-name="Group 5">
          <g data-name="Group 4">
            <path
              data-name="Path 2"
              d="M23.348 1.906h-3.487l4.006 3.643V2.425a.519.519 0 00-.519-.519z"
            />
          </g>
        </g>
      </g>
    </svg>
  );
}

export default SvgHouseGroup;
