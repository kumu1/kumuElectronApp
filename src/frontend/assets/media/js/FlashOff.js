import * as React from 'react';

function SvgFlashOff(props) {
  return (
    <svg viewBox="0 0 512 512" {...props}>
      <path d="M446.609 174.221H299.483L383.59 0H193.12L65.391 308.391h157.093L184.831 512l261.778-337.779zm-321.28 94.119l94.553-228.289h99.899l-84.107 174.221h129.225l-110.694 142.83 16.415-88.761H125.329z" />
    </svg>
  );
}

export default SvgFlashOff;
