import * as React from 'react';

function SvgFilter(props) {
  return (
    <svg width={459} height={459} {...props}>
      <path d="M178.5 382.5h102v-51h-102v51zM0 76.5v51h459v-51H0zM76.5 255h306v-51h-306v51z" />
    </svg>
  );
}

export default SvgFilter;
