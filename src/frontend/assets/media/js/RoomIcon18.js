import * as React from 'react';

function SvgRoomIcon18(props) {
  return (
    <svg viewBox="0 0 512 512" {...props}>
      <path
        d="M121 165H91V64.187l100.254-33.41 9.492 28.446L121 85.813zm0 0M76 421h360v30H76zm0 0"
        fill="#5a5a5a"
      />
      <path d="M256 421h180v30H256zm0 0" fill="#444" />
      <path d="M61 376h30v136H61zm0 0" fill="#5a5a5a" />
      <path d="M421 376h30v136h-30zm0 0" fill="#444" />
      <path d="M512 391H0l61-241h390zm0 0" fill="#96c8ef" />
      <path d="M512 391H256V150h195zm0 0" fill="#78b9eb" />
      <path d="M421 331H91l30-121h270zm0 0" fill="#ececf1" />
      <path
        d="M256 2.102C251.2.598 246.098 0 241 0c-33 0-60 27-60 60v60h120V60c0-27.898-19.2-51.3-45-57.898zm0 0"
        fill="#ff9f00"
      />
      <path d="M301 120h-45V2.102c25.8 6.597 45 30 45 57.898zm0 0" fill="#ff7816" />
      <path d="M421 331H256V210h135zm0 0" fill="#e2e2e7" />
    </svg>
  );
}

export default SvgRoomIcon18;
