import * as React from 'react';

function SvgLampSwitch(props) {
  return (
    <svg height={512} width={512} {...props}>
      <path d="M144 16h224v256H144z" fill="#ffda44" />
      <circle cx={328} cy={320} fill="#a0815c" r={16} />
      <path d="M352 496H160l8-32h176z" fill="#5f4d37" />
      <path d="M240 272h32v192h-32z" fill="#a0815c" />
      <path d="M368 8H144a8 8 0 00-8 8v256a8 8 0 008 8h88v176h-24v-16a8 8 0 00-16 0v16h-24a8 8 0 00-7.76 6.06l-8 32A8 8 0 00160 504h192a8 8 0 007.76-9.94l-8-32A8 8 0 00344 456h-64V280h40v17.38a24 24 0 1016 0V280h32a8 8 0 008-8V16a8 8 0 00-8-8zm-30.25 464l4 16h-171.5l4-16zM248 456V280h16v176zm80-128a8 8 0 118-8 8.011 8.011 0 01-8 8zm32-64H152V24h208z" />
    </svg>
  );
}

export default SvgLampSwitch;
