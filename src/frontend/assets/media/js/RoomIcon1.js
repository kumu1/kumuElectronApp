import * as React from 'react';

function SvgRoomIcon1(props) {
  return (
    <svg height={512} width={512} {...props}>
      <path d="M30 456.469H0v-58.697l30-6z" fill="#754727" data-original="#754727" />
      <path d="M512 456.469h-30v-64.697l30 6z" fill="#492d1a" data-original="#492D1A" />
      <path
        d="M63.639 55.531c-17.6 0-31.867 14.267-31.867 31.867v133.32l224.228 10 10-107.983-10-67.203H63.639z"
        fill="#d19a75"
        data-original="#D19A75"
      />
      <path
        d="M448.361 55.531H256v175.187l224.228-10V87.398c0-17.599-14.267-31.867-31.867-31.867z"
        fill="#c47e4e"
        data-original="#C47E4E"
      />
      <path
        d="M95.461 210.718v-31.867c0-17.6 14.267-31.867 31.867-31.867h96.805c17.6 0 31.867 14.267 31.867 31.867L266 202l-10 18.718z"
        fill="#fff5f5"
        data-original="#FFF5F5"
      />
      <path
        d="M256 220.718v-41.867c0-17.6 14.267-31.867 31.867-31.867h96.805c17.6 0 31.867 14.267 31.867 31.867v31.867z"
        fill="#194c80"
        data-original="#EBE1DC"
        data-old_color="#ebe1dc"
      />
      <path
        d="M37.49 210.718c-17.6 0-31.867 14.267-31.867 31.867s14.267 31.867 31.867 31.867l218.51 10 10-37.786-10-35.949H37.49z"
        fill="#194c80"
        data-original="#EBE1DC"
        data-old_color="#ebe1dc"
      />
      <path
        d="M474.51 210.718H256v73.734l218.51-10c17.6 0 31.867-14.267 31.867-31.867.001-17.6-14.267-31.867-31.867-31.867z"
        fill="#c12020"
        data-original="#C8BEB9"
        className="room-icon-1_svg__active-path"
        data-old_color="#c8beb9"
      />
      <g>
        <path d="M0 274.452v123.32h256l10-61.66-10-61.66z" fill="#d19a75" data-original="#D19A75" />
        <path d="M256 274.452h256v123.319H256z" fill="#c47e4e" data-original="#C47E4E" />
      </g>
    </svg>
  );
}

export default SvgRoomIcon1;
