/* eslint-disable react/prop-types */
import React from 'react';
import '../assets/stylesheets/index.css';
import '../assets/stylesheets/material-textfield.css';
import { NAVIGATOR_VIEW_COMPONENT } from './views';
import { SERVER_PORT } from '../constants';
import { connect } from 'react-redux';
import { GeneralReducerName } from '../redux/general';
import { DeclareSockets } from '../sockets';
import { Loader } from '../components/loader';
import { SetSocket } from '../redux/general/action';

class Navigator extends React.Component {
  state = {
    sesion: false,
  };

  componentDidMount() {
    const { io } = window;
    const { dispatch } = this.props;
    console.log(this.props);
    const url = `${window.location.protocol}//${window.location.hostname}${SERVER_PORT}`;
    const wsServer = io(url);
    // const json = JSON.parse(message.data);
    // console.log(json)
    DeclareSockets({ wsServer, dispatch });

    dispatch(SetSocket(wsServer));
  }

  render() {
    const { loading, view } = this.props;
    if (loading) {
      return <Loader />;
    }
    return NAVIGATOR_VIEW_COMPONENT[view];
  }
}
const mapStateToProps = (state) => {
  return {
    ...{
      loading: state[GeneralReducerName].loading,
      view: state[GeneralReducerName].view,
    },
  };
};
export default connect(mapStateToProps)(Navigator);
