import React from 'react';
import { NAVIGATOR_VIEW_CONSTANTS } from './constants';
import LoginContainer from '../containers/login';
import WifiNetContainer from '../containers/wifiNets';
import QrContainer from '../containers/qr';
import UsbContainer from '../containers/usb';

export const NAVIGATOR_VIEW_COMPONENT = {
  [NAVIGATOR_VIEW_CONSTANTS.LOGIN_VIEW]: <LoginContainer />,
  [NAVIGATOR_VIEW_CONSTANTS.WIFI_NETS_VIEW]: <WifiNetContainer />,
  [NAVIGATOR_VIEW_CONSTANTS.QR_INIT_CONFIG_VIEW]: <QrContainer />,
  [NAVIGATOR_VIEW_CONSTANTS.INSERT_USB_VIEW]: <UsbContainer />,
};
