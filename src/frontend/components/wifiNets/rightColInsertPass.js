import React, { useState } from 'react';
import styled from 'styled-components';
import { colors } from '../../utils/colors';
import { strings } from '../../utils/strings';
import { WifiNetButton } from '../button/wifi';
import { SquareButton } from '../button/square';
import SvgLeftArrow from '../../assets/media/js/LeftArrow';
import { PasswordOutLineTextField } from '../input/outline';
import Animation from '../../assets/media/animations/wifiLoader.gif';

const Container = styled.div`
  width: 50vw;
  height: 100vh;
  background-color: ${colors['white-grey']};
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;
const Text = styled.p`
  font-family: ComicSansMS;
  text-align: center;
  font-size: 2vw;
  color: black;
`;
const TitleContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  margin-bottom: 2vh;
  padding-right: 3%;
`;
const LeftArrowContainer = styled.div`
  cursor: pointer;
`;
const EmptySpace = styled.div`
  width: 45vw;
  height: 4vh;
`;
export const InsertWifiPassRightCol = (props) => {
  let [pass, setPass] = useState('');
  if (props.netConnecting) {
    return (
      <Container>
        <img src={Animation} />
      </Container>
    );
  }
  return (
    <Container>
      <TitleContainer>
        <LeftArrowContainer onClick={props.backPress}>
          <SvgLeftArrow fill={colors.purple} width={'2vw'} height={'2vw'} />
        </LeftArrowContainer>
        <Text style={{ marginLeft: '2vw' }}>{strings.InsertWifiPassRightCol_COMPONENT.title}</Text>
      </TitleContainer>
      <WifiNetButton data={props.net} />
      <PasswordOutLineTextField
        label={strings.InsertWifiPassRightCol_COMPONENT.pass}
        value={pass}
        onChangeText={(e) => {
          setPass(e.target.value);
        }}
      />
      <EmptySpace />
      <SquareButton
        onClick={() => props.handleConnectToWifiPress(pass)}
        text={strings.InsertWifiPassRightCol_COMPONENT.connect}
      />
    </Container>
  );
};
InsertWifiPassRightCol.defaultProps = {
  net: {},
  backPress: () => {},
  handleConnectToWifiPress: () => {},
  netConnecting: false,
};
