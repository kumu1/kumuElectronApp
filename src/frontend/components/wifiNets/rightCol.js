import React from 'react';
import styled from 'styled-components';
import { colors } from '../../utils/colors';
import { strings } from '../../utils/strings';
import { WifiNetButton } from '../button/wifi';
import Animation from '../../assets/media/animations/wifiLoader.gif';
import { SquareButton } from '../button/square';
import SvgRefresh from '../../assets/media/js/Refresh';

const Container = styled.div`
  width: 50vw;
  height: 100vh;
  background-color: ${colors['white-grey']};
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;
const NetsContainer = styled.div`
  width: 80%;
  height: 70vh;
  overflow: auto;
  display: flex;
  align-items: center;
  flex-direction: column;
`;
const Text = styled.p`
  font-family: ComicSansMS;
  text-align: center;
  font-size: 2vw;
  color: black;
`;
const TextNoDetected = styled.p`
  font-family: ComicSansMS;
  text-align: center;
  font-size: 2vw;
  color: ${colors.purple};
  width: 70%;
`;
const TitleContainer = styled.div`
  width: 69%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 2vh;
  padding-right: 3%;
`;
const RefreshContainer = styled.div`
  cursor: pointer;
`;
const EmptySpace = styled.div`
  width: 100%;
  height: 6vh;
  padding-top: 3vh;
`;

export const WifiNetRightCol = (props) => {
  if (props.wifiNets.length < 1 && !props.loaded) {
    return (
      <Container>
        <img src={Animation} />
        <Text onClick={props.skipPressed} style={{ cursor: 'pointer' }}>
          {strings.WifiNetRightCol_COMPONENT.skip}
        </Text>
      </Container>
    );
  } else if (props.wifiNets.length < 1 && props.loaded) {
    return (
      <Container>
        <TextNoDetected>
          {strings.WifiNetRightCol_COMPONENT.notWifiDetected1}
          <br />
          <br />
          {strings.WifiNetRightCol_COMPONENT.notWifiDetected2}
          <br />
          <br />
          {strings.WifiNetRightCol_COMPONENT.notWifiDetected3}
          <br />
          {strings.WifiNetRightCol_COMPONENT.notWifiDetected4}
        </TextNoDetected>
        <br />
        <Text onClick={props.skipPressed} style={{ cursor: 'pointer' }}>
          {strings.WifiNetRightCol_COMPONENT.skip}
        </Text>
        <br />
        <SquareButton onClick={props.ScanWifiNets} text={strings.WifiNetRightCol_COMPONENT.again} />
      </Container>
    );
  }
  return (
    <Container>
      <TitleContainer>
        <Text onClick={props.skipPressed} style={{ cursor: 'pointer' }}>
          {strings.WifiNetRightCol_COMPONENT.skip}
        </Text>
        <RefreshContainer onClick={props.ScanWifiNets}>
          <SvgRefresh fill={colors.purple} width={'2vw'} height={'2vw'} />
        </RefreshContainer>
      </TitleContainer>
      <NetsContainer>
        <EmptySpace />
        {props.wifiNets.map((net, index) => {
          return (
            <WifiNetButton
              key={`WifiNetRightCol_WifiNetButton${index}`}
              onClick={() => {
                props.handleNetSelected(net);
              }}
              data={net}
            />
          );
        })}
        <EmptySpace />
      </NetsContainer>
      <Text style={{ cursor: 'pointer' }}>Skip</Text>
    </Container>
  );
};
WifiNetRightCol.defaultProps = {
  wifiNets: [],
  loaded: false,
  ScanWifiNets: () => {},
  handleNetSelected: () => {},
};
