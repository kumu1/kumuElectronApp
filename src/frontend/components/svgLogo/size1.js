import React from 'react';
import styled from 'styled-components';
import HouseIcon from '../../assets/media/js/House';

const Container = styled.div`
  width: 20vw;
  height: 20vw;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
`;
const Text = styled.p`
  font-family: ComicSansMS;
  font-size: 8.5vw;
  padding-top: 4vw;
  padding-right: 1vw;
  color: white;
  position: absolute;
`;

export const SvgLogoSize1 = (props) => {
  return (
    <Container>
      <HouseIcon width={'20vw'} height={'20vw'} />
      <Text>Kumu</Text>
    </Container>
  );
};
