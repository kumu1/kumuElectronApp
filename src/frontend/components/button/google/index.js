import React from 'react';
import styled from 'styled-components';
import Ripples from 'react-ripples';
import GoogleIcon from '../../../assets/media/js/Google';
import { colors } from '../../../utils/colors';
import { strings } from '../../../utils/strings';

const Container = styled.div`
  width: ${({ width }) => width};
  height: 10vh;
  border-radius: 1vw;
  box-shadow: 0px 1px 2px 0px rgba(0, 0, 0, 0.15);
  overflow: hidden;
  cursor: pointer;
  &:hover {
    box-shadow: 0px 1px 2px 0px rgb(157, 34, 186, 0.4);
  }
`;
const SubContainer = styled.div`
  width: ${({ width }) => width};
  height: 10vh;
  background-color: white;
  border-radius: 1vw;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  padding-left: 5%;
`;
const Text = styled.p`
  font-family: arial;
  font-size: 2vw;
  color: ${colors['black-grey']};
  margin-left: 3%;
`;

export const GoogleAuthButton = (props) => {
  return (
    <Container>
      <Ripples>
        <SubContainer width={props.width}>
          <GoogleIcon width={'5vh'} height={'5vh'} />
          <Text>{strings.LOGIN_VIEW.googleSingIn}</Text>
        </SubContainer>
      </Ripples>
    </Container>
  );
};
GoogleAuthButton.defaultProps = {
  width: '33.4vw',
};
