import React from 'react';
import Ripples from 'react-ripples';
import styled from 'styled-components';
import { colors } from '../../../utils/colors';

const Container = styled.div`
  width: ${({ width }) => width};
  background-color: white;
  height: 8vh;
  box-shadow: 0px 1px 2px 0px rgba(0, 0, 0, 0.15);
  border-radius: 0.2vw;
  cursor: pointer;
`;
const SubContainer = styled.div`
  width: ${({ width }) => width};
  height: 8vh;
  background-color: white;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px solid ${colors.purple};
  border-radius: 0.2vw;
  &:hover {
    border: 2px solid ${colors.purple};
  }
`;
const Text = styled.p`
  font-family: ComicSansMS;
  text-align: center;
  font-size: 2vw;
  color: ${colors.purple};
  margin-left: 3%;
`;
export const SquareButton = (props) => {
  return (
    <Container>
      <Ripples>
        <SubContainer onClick={props.onClick} width={props.width}>
          <Text>{props.text}</Text>
        </SubContainer>
      </Ripples>
    </Container>
  );
};
SquareButton.defaultProps = {
  width: '34.7vw',
  text: 'CHANGE TEXT',
  onClick: () => {},
};
