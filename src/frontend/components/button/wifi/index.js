import React from 'react';
import styled from 'styled-components';
import { colors } from '../../../utils/colors';
import Ripples from 'react-ripples';
import SvgWifi from '../../../assets/media/js/Wifi';
const json = {
  ssid: 'TeleCentro-800a',
  bssid: 'A0:39:EE:9B:80:0F',
  mac: 'A0:39:EE:9B:80:0F',
  mode: 'Infra',
  channel: 1,
  frequency: 2412,
  signal_level: -50,
  quality: 100,
  security: 'WPA2',
  security_flags: { wpa: '(ninguno)', rsn: 'pair_tkip pair_ccmp group_tkip psk' },
};

const Container = styled.div`
  width: ${({ width }) => width};
  height: 8vh;
  box-shadow: 0px 1px 2px 0px rgba(0, 0, 0, 0.15);
  border-radius: 0.2vw;
  cursor: pointer;
  margin-top: 2vh;
  &:hover {
    box-shadow: 0px 1px 2px 0px rgb(157, 34, 186, 0.4);
  }
`;
const SubContainer = styled.div`
  width: ${({ width }) => width};
  height: 8vh;
  background-color: white;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  border-radius: 0.2vw;
  padding-left: 5%;
`;
const Text = styled.p`
  font-family: ComicSansMS;
  text-align: center;
  font-size: 1.5vw;
  color: ${colors.purple};
  margin-left: 3%;
`;
const Text2 = styled.p`
  font-family: ComicSansMS;
  text-align: center;
  font-size: 1vw;
  color: ${colors['black-grey']};
  margin-left: 3%;
`;
export const WifiNetButton = (props) => {
  const { signal_level } = props.data;
  let sig_Wifi = 0; //Como se va a representar la señal en el icono del 0(el peor) a 4 (el mejor)
  if (signal_level <= -30 && signal_level >= -67) {
    sig_Wifi = 4;
  } else if (signal_level <= -68 && signal_level >= -70) {
    sig_Wifi = 3;
  } else if (signal_level <= -71 && signal_level >= -80) {
    sig_Wifi = 2;
  } else if (signal_level <= -81 && signal_level >= -90) {
    sig_Wifi = 1;
  } else if (signal_level <= -91) {
    sig_Wifi = 0;
  }
  return (
    <Container>
      <Ripples>
        <SubContainer onClick={props.onClick} width={props.width}>
          <SvgWifi level={sig_Wifi} width={'5vh'} height={'5vh'} />
          <Text>{props.data.ssid}</Text>
          {props.data.security.length > 0 && <Text>{' - '}</Text>}
          {props.data.security.length > 0 && <Text2>{props.data.security}</Text2>}
        </SubContainer>
      </Ripples>
    </Container>
  );
};
WifiNetButton.defaultProps = {
  width: '34.7vw',
  data: json,
  onClick: () => {},
};
