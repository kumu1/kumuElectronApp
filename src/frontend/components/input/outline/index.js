import React, { useState } from 'react';
import styled from 'styled-components';
import TextField, { Input } from '@material/react-text-field';
import SvgEye from '../../../assets/media/js/Eye';
import { colors } from '../../../utils/colors';

const Container = styled.div`
  width: ${({ width }) => width};
  position: relative;
  margin-top: 4vh;
`;
export const OutLineTextField = (props) => {
  return (
    <Container width={props.width}>
      <TextField label={props.label} outlined>
        <Input
          value={props.value}
          style={{
            width: props.width,
            color: colors.purple,
          }}
          onChange={props.onChangeText}
        />
      </TextField>
    </Container>
  );
};
OutLineTextField.defaultProps = {
  width: '35vw',
  onChangeText: () => {},
  value: '',
  label: 'CHANGE LABEL',
};
export const PasswordOutLineTextField = (props) => {
  let [password, setPassword] = useState(true);
  return (
    <Container width={props.width}>
      <TextField
        label={props.label}
        outlined
        onTrailingIconSelect={() => setPassword(!password)}
        trailingIcon={
          <SvgEye
            style={{ marginBottom: '0.2vh', outline: 'none' }}
            width={'3.5vh'}
            height={'3.5vh'}
            fill={colors.purple}
          />
        }
      >
        <Input
          value={props.value}
          type={password ? 'password' : 'text'}
          style={{
            color: colors.purple,
            width: props.width,
          }}
          onChange={props.onChangeText}
        />
      </TextField>
    </Container>
  );
};
PasswordOutLineTextField.defaultProps = {
  width: '35vw',
  onChangeText: () => {},
  value: '',
  label: 'CHANGE LABEL',
};
