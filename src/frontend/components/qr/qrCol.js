import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { colors } from '../../utils/colors';
import { strings } from '../../utils/strings';
import QRCode from 'react-qr-code';
import { GeneralReducerName } from '../../redux/general';
import { SquareButton } from '../button/square';
import { CheckResetQrCodeRaspberry } from '../../redux/general/action';

const Container = styled.div`
  width: 50vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

const Text = styled.p`
  font-family: ComicSansMS;
  font-size: 2vw;
  color: ${colors.purple};
  width: 35vw;
  text-align: center;
`;
const TextB = styled.p`
  font-family: ComicSansMS;
  font-size: 2vw;
  font-weight: bold;
  color: ${colors.purple};
  width: 35vw;
  text-align: center;
`;
const TextContainer = styled.div`
  width: 80%;
  display: flex;
  justify-content: center;
  margin-bottom: 5%;
`;
const EmptySpace = styled.div`
  width: 100%;
  height: 5vh;
`;
const QrColC = (props) => {
  const HandleButtonpress = () => {
    props.dispatch(CheckResetQrCodeRaspberry());
  };
  return (
    <Container>
      <TextContainer>
        <Text>
          {strings.QrCol_COMPONENT.title}
          <TextB>{strings.QrCol_COMPONENT.title2}</TextB>
        </Text>
      </TextContainer>
      <QRCode value={props.qrHash} />
      <EmptySpace />
      <SquareButton onClick={HandleButtonpress} text={strings.QrCol_COMPONENT.check_reset} />
    </Container>
  );
};
const mapStateToProps = (state) => {
  return {
    ...{
      qrHash: state[GeneralReducerName].qrHash,
    },
  };
};
const toExport = connect(mapStateToProps)(QrColC);
export { toExport as QrCol };
