import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { colors } from '../../utils/colors';
import { strings } from '../../utils/strings';
import SvgUsb from '../../assets/media/js/Usb';
import { SquareButton } from '../button/square';
import { ActionResetRaspberryBackend } from '../../redux/general/action';

const Container = styled.div`
  width: 50vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;
const Text = styled.p`
  font-family: ComicSansMS;
  font-size: 2vw;
  color: ${colors.purple};
  width: 35vw;
  text-align: center;
  margin-bottom: 10%;
`;
const UsbContainer = styled.div`
  -webkit-animation: heartbeat 0.9s infinite both;
  animation: heartbeat 0.9s infinite both;
  margin-bottom: 5%;
`;
const UsbRotateContainer = styled.div`
  -webkit-animation: rotate-center 0.9s infinite both;
  animation: rotate-center 0.9s infinite both;
  margin-bottom: 5%;
`;

export const UsbColComponent = (props) => {
  if (props.usbConnectedLoading) {
    return (
      <Container>
        <UsbRotateContainer>
          <SvgUsb width={'25vw'} height={'25vw'} fill={colors.purple} />
        </UsbRotateContainer>
      </Container>
    );
  } else {
    return (
      <Container>
        <UsbContainer>
          <SvgUsb width={'25vw'} height={'25vw'} fill={colors.purple} />
        </UsbContainer>
        <Text>{strings.UsbCol_COMPONENT.title}</Text>
        <SquareButton
          onClick={() => {
            props.dispatch(ActionResetRaspberryBackend());
          }}
          text={strings.UsbCol_COMPONENT.addNew}
        />
      </Container>
    );
  }
};
const UsbCol = connect()(UsbColComponent);
export { UsbCol };
