import React from 'react';
import styled from 'styled-components';
import { colors } from '../../utils/colors';
import { SvgLogoSize1 } from '../svgLogo/size1';

const Container = styled.div`
  width: 50vw;
  height: 100vh;
  position: relative;
`;
const Subcontainer = styled.div`
  width: 50vw;
  height: 100vh;
  background-color: ${colors.purple};
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  position: absolute;
  top: 0;
  left: 0;
  -webkit-box-shadow: 2px 0px 6px 4px rgba(0, 0, 0, 0.35);
  -moz-box-shadow: 2px 0px 6px 4px rgba(0, 0, 0, 0.35);
  box-shadow: 2px 0px 6px 4px rgba(0, 0, 0, 0.35);
`;

export const LoginLeftCol = (props) => {
  return (
    <Container>
      <Subcontainer>
        <SvgLogoSize1 />
      </Subcontainer>
    </Container>
  );
};
