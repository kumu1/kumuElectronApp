import React from 'react';
import styled from 'styled-components';
import { colors } from '../../utils/colors';
import { OutLineTextField, PasswordOutLineTextField } from '../input/outline';
import { strings } from '../../utils/strings';
import { GoogleAuthButton } from '../button/google';
import { SquareButton } from '../button/square';

const Container = styled.div`
  width: 50vw;
  height: 100vh;
  background-color: ${colors['white-grey']};
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;
const LineSeparator = styled.div`
  width: 16vw;
  height: 0.2vw;
  background-color: ${colors['black-grey']};
`;
const CircleSeparator = styled.div`
  width: 1vw;
  height: 1vw;
  border-radius: 3vw;
  background-color: ${colors['black-grey']};
`;
const SeparatorContainer = styled.div`
  width: 35vw;
  height: 2vw;
  padding-horizontal: 11%;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 1vh;
  margin-top: 4vh;
  display: flex;
`;
const EmptySpace = styled.div`
  width: 35vw;
  height: 6vh;
`;
const EmptySpace2 = styled.div`
  width: 35vw;
  height: 2vh;
`;
const ForgoRegisterText = styled.p`
  font-family: ComicSansMS;
  font-size: 1vw;
  color: ${colors.purple};
  width: 35vw;
  text-align: center;
  cursor: pointer;
  &: hover {
    text-decoration: underline;
  }
`;

export const LoginRightCol = (props) => {
  return (
    <Container>
      <GoogleAuthButton />
      <SeparatorContainer>
        <LineSeparator />
        <CircleSeparator />
        <LineSeparator />
      </SeparatorContainer>
      <OutLineTextField label={strings.LOGIN_VIEW.email} />
      <PasswordOutLineTextField label={strings.LOGIN_VIEW.password} />
      <EmptySpace />
      <ForgoRegisterText>{strings.LOGIN_VIEW['forgot-register']}</ForgoRegisterText>
      <EmptySpace2 />
      <SquareButton text={strings.LOGIN_VIEW.button} />
    </Container>
  );
};
